<?php

// Connexion à la Base de Donnée.

DEFINE(SERVER, "localhost");
DEFINE(LOGIN, "root");
DEFINE(MDP, "root");
DEFINE(BASE, "colyseum");

$connect = mysqli_connect(SERVER, LOGIN, MDP, BASE) or die("Erreur de connexion au serveur");


// Exercice 1 : Afficher tous les clients.

    // $result = mysqli_query($connect, "SELECT * FROM clients");

    // while($data = mysqli_fetch_assoc($result)) {
    //     echo "Last Name : " . $data["lastName"] . "<br>";
    //     echo "First Name : " . $data["firstName"] . "<br>";
    //     echo "<hr>";
    // }

// Exercice 2 : Afficher tous les types de spectacles possibles.

        // $result = mysqli_query($connect, "SELECT * FROM showTypes");

        // echo "<h3>Show types :</h3>";
        // while ($data = mysqli_fetch_assoc($result)) {
        //     echo $data["type"] . "<br>"; 
        // }

// Exercice 3 : Afficher les 20 premiers clients.

    // $result = mysqli_query($connect, "SELECT * FROM clients");

    // while($data = mysqli_fetch_assoc($result)) {
    //     if ($data["id"] <= 20) {
    //         echo "Id : " . $data["id"] . "<br>";
    //         echo "Last Name : " . $data["lastName"] . "<br>";
    //         echo "First Name : " . $data["firstName"] . "<br>";
    //         echo "<hr>";
    //     }
    // }

// Exercice 4 : N'afficher que les clients possédant une carte de fidélité.

    // $result = mysqli_query($connect, "SELECT * FROM clients WHERE card = 1");

    // echo "<h3>Client with card :</h3>";

    // while($data = mysqli_fetch_assoc($result)) {
    //     echo "Last Name : " . $data["lastName"] . "<br>";
    //     echo "First Name : " . $data["firstName"] . "<br>";
    //     echo "<hr>";
    // }

// Exercice 5 : Afficher uniquement le nom et le prénom de tous les clients dont le nom commence par la lettre "M".
// Les afficher comme ceci :

// Nom : *Nom du client*
// Prénom : *Prénom du client*

// Trier les noms par ordre alphabétique.

    // $result = mysqli_query($connect, "SELECT * FROM clients WHERE lastName LIKE 'M%' ORDER BY lastName");

    // echo "<h3>Client with 'M'</h3>";

    // while($data = mysqli_fetch_assoc($result)) {
    //     echo "Nom : " . $data["lastName"] . "<br>";
    //     echo "Prénom : " . $data["firstName"] . "<br>";
    //     echo "<hr>";
    // }

// Exercice 6 : Afficher le titre de tous les spectacles ainsi que l'artiste, la date et l'heure. 
// Trier les titres par ordre alphabétique. 
// Afficher les résultat comme ceci : *Spectacle* par *artiste*, le *date* à *heure*.

    // $result = mysqli_query($connect, "SELECT * FROM shows");

    // echo "<h3>Shows</h3>";

    // while($data = mysqli_fetch_assoc($result)) {
    //     echo $data["title"] . " par " . $data["performer"] . ", le " . $data["date"] . " à " . $data["startTime"] . ".<br>";
    //     echo "<hr>";
    // }

// Exercice 7 : Afficher tous les clients comme ceci :
// Nom : *Nom de famille du client*
// Prénom : *Prénom du client*
// Date de naissance : *Date de naissance du client*
// Carte de fidélité : *Oui (Si le client en possède une) ou Non (s'il n'en possède pas)*
// Numéro de carte : *Numéro de la carte fidélité du client s'il en possède une.*

    $result = mysqli_query($connect, "SELECT * FROM clients");

    while($data = mysqli_fetch_assoc($result)) {
        echo "Nom : " . $data["lastName"] . "<br>";
        echo "Prénom : " . $data["firstName"] . "<br>";
        echo "Date de naissance : " . $data["birthDate"] . "<br>";

        echo "Carte de fidélité : "; 
        if($data["card"] == 1) {
            echo "Oui<br>";
        }
        else {
            echo "Non<br>";
        }
    
        echo "Numéro de carte : " . $data["cardNumber"] . "<br>";
        echo "<hr>";
    }


?>